import React, { useState, useEffect } from "react";
import axios from 'axios';
import Clouds from '../public/images/clouds.png';
import Drizzle from '../public/images/drizzle.png';
import Mist from '../public/images/mist.png';
import Rain from '../public/images/rain.png';
import Clear from '../public/images/clear.png';
import SearchIcon from '@mui/icons-material/Search';
export default function Home() {
  const [data, setData] = useState({});
  const [search, setSearch] = useState('');
  const [location, setLocation] = useState('kathmandu');

  const url = `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=cc965723e32c8c8fdb5638ef5d5a2896`;

  const call = () => {
    axios.get(url).then((response) => {
      setData(response.data);
      console.log(response.data);
      console.log('yo chaleko vakhar');
    });
  };

  useEffect(() => {
    if (location) {
      call();
    }
  }, [location]);

  const handleInputChange = (event) => {
    setSearch(event.target.value);
  };

  const handleClick = (event) => {
    event.preventDefault();
    console.log("searchedContent:", search);
    setLocation(search);
  };
  const FtoC = () => {

  }

  return (
    <div className="bg-[#e8e8e8] h-screen flex items-center justify-center">
      <section className="bg-gradient-to-r from-blue-400 to-purple-500 text-white p-12 rounded-lg shadow-md w-full max-w-xl">
        <form>
          <div className="mb-4">
            <div className="flex gap-6">
              <input
                type="text"
                placeholder="Search location...."
                className="w-full border text-black border-gray-300 rounded-full p-2 focus:outline-none focus:border-blue-500 shadow-sm transition duration-300 ease-in-out transform hover:scale-105"
                value={search}
                onChange={handleInputChange}
              />
              <button
                className="w-16 h-12 bg-neutral-700 text-white rounded-full hover:bg-neutral-900 focus:outline-none focus:ring-2 focus:ring-blue-400 transition duration-300 ease-in-out transform hover:scale-105"
                onClick={handleClick}
              >
                <SearchIcon />
              </button>
            </div>

          </div>

        </form>
        {data.name && (
          <>
              <div>
                <label className="text-xl font-bold">{data.name}</label><br/>
                <label className="text-xs">Updated a few minutes ago</label>  
              </div>
              <div>
                {data.weather?.[0].main=="Clouds" ?(<img src={Clouds} alt="Clouds" />):
                data.weather?.[0].main=="Rain" ?(<img src={Rain} alt="Rain" />):
                data.weather?.[0].main=="Drizzle" ?(<img src={Drizzle} alt="Drizzle" />):
                data.weather?.[0].main=="Mist" ?(<img src={Mist} alt="Mist" />):
                data.weather?.[0].main=="Clear Sky" ?(<img src={Clear} alt="Clear Sky" />):
                (<img src={random} alt={data.weather?.[0].main} />)}
              </div>
              

            <div>
              <label className="text-3xl font-bold">{data.main?.temp ? `${(data.main.temp - 273.15).toFixed(2)} °C` : 'N/A'}</label>
            </div>
          </>
        )}
      </section>
    </div>
  )
}
